﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genetyczne_Projekt
{
    public class Osobnik
    {
        private int id, x_zakodowaneDziesiętne, y_zakodowaneDziesiętne;
        private double x_rzeczywiste, y_rzeczywiste, przystosowanie /* wyliczane tylko przez ręczne wywołanie funkcji! */;
        private string x_zakodowaneBinarne, y_zakodowaneBinarne;

        public Osobnik() { }

        public Osobnik(int id, double x_wartośćRzeczywista, double y_wartośćRzeczywista)
        {
            this.id = id;
            X_WartośćRzeczywista = x_wartośćRzeczywista;
            Y_WartośćRzeczywista = y_wartośćRzeczywista;
        }

        public Osobnik(Osobnik osobnik)
        {
            this.id = osobnik.id;
            this.x_zakodowaneDziesiętne = osobnik.x_zakodowaneDziesiętne;
            this.x_rzeczywiste = osobnik.x_rzeczywiste;
            this.x_zakodowaneBinarne = osobnik.x_zakodowaneBinarne;
            this.przystosowanie = osobnik.przystosowanie;
            this.y_zakodowaneDziesiętne = osobnik.y_zakodowaneDziesiętne;
            this.y_rzeczywiste = osobnik.y_rzeczywiste;
            this.y_zakodowaneBinarne = osobnik.y_zakodowaneBinarne;
            this.przystosowanie = osobnik.przystosowanie;
        }

        

        #region Funkcje

        private int zakodujLiczbę(double liczbaOdkodowana, double początekPrzedziału, int długośćPrzedziału, int liczbaBitówOsobnika)
        {
            int liczbaZakodowana;

            //wzór: x'=[(x - a)(2^n - 1)]/d     x - wartość odkodowana,   a - początek przedziału,   d - długość przedziału,   n - ilość bitów osobnika
            
            liczbaZakodowana = Convert.ToInt32(Math.Ceiling(
                ( (liczbaOdkodowana - początekPrzedziału) * (Math.Pow(2, liczbaBitówOsobnika) - 1)) / długośćPrzedziału
                ));

            return liczbaZakodowana;
        }

        private double odkodujLiczbę(int liczbaZakodowana, double początekPrzedziału, int długośćPrzedziału, int liczbaBitówOsobnika, int dokładnośćPrzedziału)
        {
            double liczbaOdkodowana;

            //wzór: x = a + [(d * x') / (2^n - 1)]  x' - wartość zakodowana,   a - początek przedziału,   d - długość przedziału,   n - ilość bitów osobnika

            liczbaOdkodowana = (długośćPrzedziału-1) * liczbaZakodowana;
            liczbaOdkodowana /= (Math.Pow(2, liczbaBitówOsobnika)-1);
            liczbaOdkodowana += początekPrzedziału;

            liczbaOdkodowana *= Math.Pow(10, dokładnośćPrzedziału);
            liczbaOdkodowana = Math.Floor(liczbaOdkodowana);
            liczbaOdkodowana /= Math.Pow(10, dokładnośćPrzedziału);
            //liczbaOdkodowana = początekPrzedziału + (((długośćPrzedziału * liczbaZakodowana) / (Math.Pow(2, liczbaBitówOsobnika)) - 1));

            return liczbaOdkodowana;
        }

        private string przeliczNaBinarne(int liczbaDoPrzeliczenia, int liczbaBitówOsobnika)
        {
            string liczbaPrzeliczona = "";

            for (int i = 0; i < liczbaBitówOsobnika; i++)
            {
                if (liczbaDoPrzeliczenia % 2 == 0) liczbaPrzeliczona += '0';
                else liczbaPrzeliczona += '1';

                liczbaDoPrzeliczenia /= 2;
            }

            //odwracanie stringa
            string tmp = liczbaPrzeliczona;
            liczbaPrzeliczona = "";
            for (int i = tmp.Length - 1; i >= 0; i--)
            {
                liczbaPrzeliczona += tmp[i];
            }

            return liczbaPrzeliczona;
        }

        private int przeliczNaDziesiętne(string liczbaDoPrzeliczenia)
        {
            int liczbaDziesiętna = 0;

            for (int i = 0; i < liczbaDoPrzeliczenia.Length; i++)
            {
                if (liczbaDoPrzeliczenia[i] == '1') liczbaDziesiętna += Convert.ToInt32(Math.Pow(2, liczbaDoPrzeliczenia.Length - i - 1));
            }

            return liczbaDziesiętna;
        }

        public void obliczPrzystosowanie()
        {
            double przystosowanie;
            
            przystosowanie = Menu_Główne.obliczWartośćFunkcji(x_rzeczywiste, y_rzeczywiste, Menu_Główne.funkcja);

            this.przystosowanie = przystosowanie;
        }
        #endregion


        #region Właściwości

        public int Id
        {
            get
            {
                return id;
            }
        }

        public double X_WartośćRzeczywista
        {
            get
            {
                return x_rzeczywiste;
            }
            set
            {
                x_rzeczywiste = poprawWartośćRzeczywistą(value, Menu_Główne.dokładnośćX);

                //x_rzeczywiste = Math.Round(value, Menu_Główne.dokładnośćX);
                x_zakodowaneDziesiętne = zakodujLiczbę(x_rzeczywiste, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX);
                x_zakodowaneBinarne = przeliczNaBinarne(x_zakodowaneDziesiętne, Menu_Główne.liczbaBitówOsobnikaX);
                przystosowanie = double.NaN;    //zabezpieczenie przed stanem niespójnym 
            }
        }

        public int X_WartośćZakodowanaDziesiętna
        {
            get
            {
                return x_zakodowaneDziesiętne;
            }
            set
            {
                x_zakodowaneDziesiętne = value;

                x_rzeczywiste = odkodujLiczbę(x_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX, Menu_Główne.dokładnośćX);
                //x_rzeczywiste = poprawWartośćRzeczywistą(odkodujLiczbę(x_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX), Menu_Główne.dokładnośćX);
                
                //x_rzeczywiste = Math.Round(odkodujLiczbę(x_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX), Menu_Główne.dokładnośćX);
                x_zakodowaneBinarne = przeliczNaBinarne(x_zakodowaneDziesiętne, Menu_Główne.liczbaBitówOsobnikaX);
                przystosowanie = double.NaN;    //zabezpieczenie przed stanem niespójnym
            }
        }

        public string X_WartośćZakodowanaBinarna
        {
            get 
            {
                return x_zakodowaneBinarne;
            }
            set
            {
                x_zakodowaneBinarne = value;
                x_zakodowaneDziesiętne = przeliczNaDziesiętne(x_zakodowaneBinarne);

                x_rzeczywiste = odkodujLiczbę(x_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX, Menu_Główne.dokładnośćX);
                //x_rzeczywiste = poprawWartośćRzeczywistą(odkodujLiczbę(x_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX), Menu_Główne.dokładnośćX);
                
                //x_rzeczywiste = Math.Round(odkodujLiczbę(x_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuX, Menu_Główne.długośćPrzedziałuX, Menu_Główne.liczbaBitówOsobnikaX), Menu_Główne.dokładnośćX);
                przystosowanie = double.NaN;    //zabezpieczenie przed stanem niespójnym
            }
        }


        public double Y_WartośćRzeczywista
        {
            get
            {
                return y_rzeczywiste;
            }
            set
            {
                y_rzeczywiste = poprawWartośćRzeczywistą(value, Menu_Główne.dokładnośćY);

                //y_rzeczywiste = Math.Round(value, Menu_Główne.dokładnośćY);
                y_zakodowaneDziesiętne = zakodujLiczbę(y_rzeczywiste, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY);
                y_zakodowaneBinarne = przeliczNaBinarne(y_zakodowaneDziesiętne, Menu_Główne.liczbaBitówOsobnikaY);
                przystosowanie = double.NaN;    //zabezpieczenie przed stanem niespójnym 
            }
        }

        public int Y_WartośćZakodowanaDziesiętna
        {
            get
            {
                return y_zakodowaneDziesiętne;
            }
            set
            {
                y_zakodowaneDziesiętne = value;

                y_rzeczywiste = odkodujLiczbę(y_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY, Menu_Główne.dokładnośćY);
                //y_rzeczywiste = poprawWartośćRzeczywistą(odkodujLiczbę(y_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY), Menu_Główne.dokładnośćY);

                //y_rzeczywiste = Math.Round(odkodujLiczbę(y_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY), Menu_Główne.dokładnośćY);
                y_zakodowaneBinarne = przeliczNaBinarne(y_zakodowaneDziesiętne, Menu_Główne.liczbaBitówOsobnikaY);
                przystosowanie = double.NaN;    //zabezpieczenie przed stanem niespójnym
            }
        }

        public string Y_WartośćZakodowanaBinarna
        {
            get
            {
                return y_zakodowaneBinarne;
            }
            set
            {
                y_zakodowaneBinarne = value;
                y_zakodowaneDziesiętne = przeliczNaDziesiętne(y_zakodowaneBinarne);

                y_rzeczywiste = odkodujLiczbę(y_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY, Menu_Główne.dokładnośćY);
                //y_rzeczywiste = poprawWartośćRzeczywistą(odkodujLiczbę(y_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY), Menu_Główne.dokładnośćY);

                //y_rzeczywiste = Math.Round(odkodujLiczbę(y_zakodowaneDziesiętne, Menu_Główne.początekPrzedziałuY, Menu_Główne.długośćPrzedziałuY, Menu_Główne.liczbaBitówOsobnikaY), Menu_Główne.dokładnośćY);
                przystosowanie = double.NaN;    //zabezpieczenie przed stanem niespójnym
            }
        }

        private double poprawWartośćRzeczywistą(double surowaWartośćOdkodowana, int dokładność)
        {
            double wartośćRzeczywista = surowaWartośćOdkodowana;
            wartośćRzeczywista *= Math.Pow(10, dokładność);
            wartośćRzeczywista = Math.Floor(wartośćRzeczywista);
            wartośćRzeczywista /= Math.Pow(10, dokładność);
            return wartośćRzeczywista;
        }



        public double Przystosowanie
        {
            get
            {
                return przystosowanie;
            }
        }

        #endregion
    }
}
