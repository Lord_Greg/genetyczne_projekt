﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genetyczne_Projekt
{
    public class Składnik_Funkcji
    {
        public int id;
        public double a, potęgaX, potęgaY;

        public Składnik_Funkcji(int id, double a)
        {
            this.id = id;
            this.a = a;
            potęgaX = 0;
            potęgaY = 0;
        }

        public Składnik_Funkcji(int id, double a, double potęgaX, double potęgaY)
        {
            this.id = id;
            this.a = a;
            this.potęgaX = potęgaX;
            this.potęgaY = potęgaY;
        }

        public Składnik_Funkcji(Składnik_Funkcji składnik)
        {
            this.id = składnik.id;
            this.a = składnik.a;
            this.potęgaX = składnik.potęgaX;
            this.potęgaY = składnik.potęgaY;
        }


        public double obliczWartośćSkładnika(double x, double y)
        {
            /*
            if (a == 0 || x == 0 || y == 0) //inaczej czasami wywalała błąd przy liczeniu wyniku
            {
                return 0;
            }
            */
            double wynik;
            wynik = a * Math.Pow(x, potęgaX) * Math.Pow(y, potęgaY);
            return wynik;
        }
    }
}
