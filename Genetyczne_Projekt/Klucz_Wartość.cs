﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Genetyczne_Projekt
{
    public class Klucz_Wartość
    {
        private int klucz, wartość;

        public Klucz_Wartość() {}

        public Klucz_Wartość(int klucz, int wartość)
        {
            this.klucz = klucz;
            this.wartość = wartość;
        }

        public int Klucz
        {
            get { return klucz; }
        }

        public int Wartość
        {
            get { return wartość; }
            set { wartość = value; }
        }
    }
}
