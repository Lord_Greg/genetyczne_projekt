﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Genetyczne_Projekt
{
    public partial class Menu_Główne : Form
    {
        public static double początekPrzedziałuX, koniecPrzedziałuX, początekPrzedziałuY, koniecPrzedziałuY;
        public static int liczbaOsobniów, liczbaIteracji, dokładnośćX, dokładnośćY /* Liczba miejsc po przecinku */, bitKrzyżowaniaX, bitKrzyżowaniaY, bitMutowaniaX, bitMutowaniaY, prawdopodobieństwoKrzyżowaniaX, prawdopodobieństwoKrzyżowaniaY, prawdopodobieństwoMutowaniaX, prawdopodobieństwoMutowaniaY,
                          /* wyliczane: */ liczbaBitówOsobnikaX, długośćPrzedziałuX, liczbaBitówOsobnikaY, długośćPrzedziałuY,
                          któryToSkładnikFunkcji=0;
        public bool czyPoprawnyPoczątekPrzedziałuX, czyPoprawnyKoniecPrzedziałuX, czyPoprawnyPoczątekPrzedziałuY, czyPoprawnyKoniecPrzedziałuY;
        public static Random losuj = new Random();
        public static List<Składnik_Funkcji> funkcja = new List<Składnik_Funkcji>(), pochodna;
        public static List<Osobnik> populacja;
        public List<Klucz_Wartość> listaIdWybranychOsobników;
        Osobnik najlepszyOsobnik;
        int liczbaWykonanychIteracji;

        //min. Liczba osobników = 4

        public Menu_Główne()
        {
            InitializeComponent();

            rtbWyniki.Height = this.Height - rtbWyniki.Top - 50;
            rtbWyniki.Width = this.Width - rtbWyniki.Left - 20;

            inicjalizujZmienne();
            lblFunkcja.Left = lblFodXY.Right;
            chbKontynuuj.Checked = false;
            chbKontynuuj.Enabled = false;
            chbKontynuuj.Visible = false;
        }



        #region Zdarzenia

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (funkcja.Count > 0)
            {
                if (!chbKontynuuj.Checked)
                {
                    liczbaWykonanychIteracji = 0;
                    listaIdWybranychOsobników = new List<Klucz_Wartość>();
                    rtbWyniki.Text = "Poszukiwania maksimum funkcji f(x,y)=" + lblFunkcja.Text + "\n\n";

                    generujPopulację(out populacja); //nowa populacja
                    dopiszDoRichTexta(populacja, 0);
                }
                działaj(liczbaIteracji);
                chbKontynuuj.Enabled = true;
                chbKontynuuj.Checked = true;
                chbKontynuuj.Visible = true;
            }
            else
            {
                MessageBox.Show("Podaj jakąś funkcje");
            }


            //Testy:
            //MessageBox.Show( Osobnik.przeliczNaDziesiętne(Osobnik.przeliczNaBinarne(52)).ToString() );
        }

        private void działaj(int liczbaIteracjiDoWykonania)
        {
            for (int i = 0; i < liczbaIteracjiDoWykonania; i++)
            {

                if (liczbaWykonanychIteracji > 0)
                {
                    generujPopulację(populacja, listaIdWybranychOsobników);
                }

                


                selekcja(out listaIdWybranychOsobników, populacja);
                mutacja(listaIdWybranychOsobników, populacja);
                krzyżowanie(listaIdWybranychOsobników, populacja);

                dopiszDoRichTexta(populacja, liczbaWykonanychIteracji + i + 1);
            }
            liczbaWykonanychIteracji += liczbaIteracjiDoWykonania;

            //dopiszDoRichTexta(populacja, liczbaIteracji);
            rtbWyniki.Text += "_________________________________________\n";
            rtbWyniki.Text += "\nNajlepsze wartości po " + liczbaWykonanychIteracji.ToString() + " iteracjach to:\n\tx=" + najlepszyOsobnik.X_WartośćRzeczywista.ToString() + "\n\ty=" + najlepszyOsobnik.Y_WartośćRzeczywista.ToString() + "\n\tf(x,y)=" + najlepszyOsobnik.Przystosowanie.ToString() + "\n\n";
        }

        #region Tworzenie wzoru funkcji
        private void btnFunkcjaDodaj_Click(object sender, EventArgs e)
        {
            double a, potęgaX, potęgaY;
            if (Double.TryParse(tbxFunkcjaA.Text, out a) && Double.TryParse(tbxFunkcjaPotęgaX.Text, out potęgaX) && Double.TryParse(tbxFunkcjaPotęgaY.Text, out potęgaY))
            {
                if (któryToSkładnikFunkcji == 0)
                {
                    lblFunkcja.Text = "";
                    funkcja = new List<Składnik_Funkcji>();
                    pochodna = new List<Składnik_Funkcji>();
                }

                któryToSkładnikFunkcji++;
                Składnik_Funkcji nowySkładnik = new Składnik_Funkcji(któryToSkładnikFunkcji, a, potęgaX, potęgaY);
                string tekstSkładnika = "";
                if (a >= 0 && któryToSkładnikFunkcji > 1)
                {
                    tekstSkładnika += "+";
                }
                tekstSkładnika += tbxFunkcjaA.Text + "x^(" + tbxFunkcjaPotęgaX.Text + ")y^(" + tbxFunkcjaPotęgaY.Text + ") ";
                lblFunkcja.Text += tekstSkładnika;
                funkcja.Add(new Składnik_Funkcji(nowySkładnik));
                nowySkładnik.a *= nowySkładnik.potęgaY;
                nowySkładnik.potęgaY--;
                pochodna.Add(new Składnik_Funkcji(nowySkładnik));
            }
        }

        private void btnFunkcjaCofnij_Click(object sender, EventArgs e)
        {
            if (któryToSkładnikFunkcji > 0)
            {
                if (któryToSkładnikFunkcji == 1)
                {
                    lblFunkcja.Text = "";
                    funkcja = new List<Składnik_Funkcji>();
                    pochodna = new List<Składnik_Funkcji>();
                }
                else
                {
                    funkcja.Remove(funkcja.Find(skladnik => skladnik.id == któryToSkładnikFunkcji));
                    pochodna.Remove(pochodna.Find(skladnik => skladnik.id == któryToSkładnikFunkcji));

                    string funkcjaWyświetlana = lblFunkcja.Text;
                    int długośćNowejFunkcji = funkcjaWyświetlana.Length - 1;
                    do
                    {
                        długośćNowejFunkcji--;

                    } while (funkcjaWyświetlana[długośćNowejFunkcji] != ' ');

                    lblFunkcja.Text = kopiujStringa(lblFunkcja.Text, 0, długośćNowejFunkcji);
                }
                któryToSkładnikFunkcji--;
            }

        }
        #endregion

        


        #region Testy i ustawianie wartości wprowadzonych przedziałów
        private void tbxPoczątekPrzedziałuX_Leave(object sender, EventArgs e)
        {
            czyPoprawnyPoczątekPrzedziałuX = testujPoczątekPrzedziału(tbxPoczątekPrzedziałuX, koniecPrzedziałuX);
            if (czyPoprawnyPoczątekPrzedziałuX)
            {
                double nowaWartość;
                bool czyNapewnoZmienić = true;

                Double.TryParse(tbxPoczątekPrzedziałuX.Text, out nowaWartość);

                if (chbKontynuuj.Checked && nowaWartość > początekPrzedziałuX)
                {
                    czyNapewnoZmienić = this.czyNapewnoZmienić();
                }

                if (czyNapewnoZmienić)
                {
                    początekPrzedziałuX = nowaWartość;
                    długośćPrzedziałuX = ustalDługośćPrzedziału(początekPrzedziałuX, koniecPrzedziałuX, dokładnośćX);
                    liczbaBitówOsobnikaX = ustalLiczbęBitów(długośćPrzedziałuX);
                    nudBitKrzyżowaniaX.Maximum = liczbaBitówOsobnikaX;
                    nudBitMutowaniaX.Maximum = liczbaBitówOsobnikaX;
                }
                else
                {
                    tbxPoczątekPrzedziałuX.Text = Math.Round(początekPrzedziałuX, dokładnośćX).ToString();
                }
                
            }
        }

        private void tbxKoniecPrzedziałuX_Leave(object sender, EventArgs e)
        {
            czyPoprawnyKoniecPrzedziałuX = testujKoniecPrzedziału(tbxKoniecPrzedziałuX, początekPrzedziałuX);
            if (czyPoprawnyKoniecPrzedziałuX)
            {
                double nowaWartość;
                bool czyNapewnoZmienić = true;

                Double.TryParse(tbxKoniecPrzedziałuX.Text, out nowaWartość);

                if (chbKontynuuj.Checked && nowaWartość < koniecPrzedziałuX)
                {
                    czyNapewnoZmienić = this.czyNapewnoZmienić();
                }

                if (czyNapewnoZmienić)
                {
                    koniecPrzedziałuX = nowaWartość;
                    długośćPrzedziałuX = ustalDługośćPrzedziału(początekPrzedziałuX, koniecPrzedziałuX, dokładnośćX);
                    liczbaBitówOsobnikaX = ustalLiczbęBitów(długośćPrzedziałuX);
                    nudBitKrzyżowaniaX.Maximum = liczbaBitówOsobnikaX;
                    nudBitMutowaniaX.Maximum = liczbaBitówOsobnikaX;
                }
                else
                {
                    tbxKoniecPrzedziałuX.Text = Math.Round(koniecPrzedziałuX, dokładnośćX).ToString();
                }

            }
        }

        private void tbxPoczątekPrzedziałuY_Leave(object sender, EventArgs e)
        {
            czyPoprawnyPoczątekPrzedziałuY = testujPoczątekPrzedziału(tbxPoczątekPrzedziałuY, koniecPrzedziałuY);
            if (czyPoprawnyPoczątekPrzedziałuY)
            {
                double nowaWartość;
                bool czyNapewnoZmienić = true;

                Double.TryParse(tbxPoczątekPrzedziałuY.Text, out nowaWartość);

                if (chbKontynuuj.Checked && nowaWartość > początekPrzedziałuY)
                {
                    czyNapewnoZmienić = this.czyNapewnoZmienić();
                }

                if (czyNapewnoZmienić)
                {
                    początekPrzedziałuY = nowaWartość;
                    długośćPrzedziałuY = ustalDługośćPrzedziału(początekPrzedziałuY, koniecPrzedziałuY, dokładnośćY);
                    liczbaBitówOsobnikaY = ustalLiczbęBitów(długośćPrzedziałuY);
                    nudBitKrzyżowaniaY.Maximum = liczbaBitówOsobnikaY;
                    nudBitMutowaniaY.Maximum = liczbaBitówOsobnikaY;
                }
                else
                {
                    tbxPoczątekPrzedziałuY.Text = Math.Round(początekPrzedziałuY, dokładnośćY).ToString();
                }

            }
        }

        private void tbxKoniecPrzedziałuY_Leave(object sender, EventArgs e)
        {
            czyPoprawnyKoniecPrzedziałuY = testujKoniecPrzedziału(tbxKoniecPrzedziałuY, początekPrzedziałuY);
            if (czyPoprawnyKoniecPrzedziałuY)
            {
                double nowaWartość;
                bool czyNapewnoZmienić = true;

                Double.TryParse(tbxKoniecPrzedziałuY.Text, out nowaWartość);
                
                if (chbKontynuuj.Checked && nowaWartość < koniecPrzedziałuY)
                {
                    czyNapewnoZmienić = this.czyNapewnoZmienić();
                }

                if (czyNapewnoZmienić)
                {
                    koniecPrzedziałuY = nowaWartość;
                    długośćPrzedziałuY = ustalDługośćPrzedziału(początekPrzedziałuY, koniecPrzedziałuY, dokładnośćY);
                    liczbaBitówOsobnikaY = ustalLiczbęBitów(długośćPrzedziałuY);
                    nudBitKrzyżowaniaY.Maximum = liczbaBitówOsobnikaY;
                    nudBitMutowaniaY.Maximum = liczbaBitówOsobnikaY;
                }
                else
                {
                    tbxKoniecPrzedziałuY.Text = Math.Round(koniecPrzedziałuY, dokładnośćY).ToString();
                }

            }
        }
        #endregion

        #region Ustawianie zmiennych po zmianie w numericach
        private void nudLiczbaOsobnikow_Leave(object sender, EventArgs e)
        {
            liczbaOsobniów = Convert.ToInt32(nudLiczbaOsobnikow.Value);
        }
        
        private void nudLiczbaOsobnikow_ValueChanged(object sender, EventArgs e)
        {
            liczbaOsobniów = Convert.ToInt32(nudLiczbaOsobnikow.Value);
        }


        private void nudLiczbaIteracji_Leave(object sender, EventArgs e)
        {
            liczbaIteracji = Convert.ToInt32(nudLiczbaIteracji.Value);
        }


        private void nudDokładnośćX_Leave(object sender, EventArgs e)
        {
            dokładnośćX = Convert.ToInt32(nudDokładnośćX.Value);
            długośćPrzedziałuX = ustalDługośćPrzedziału(początekPrzedziałuX, koniecPrzedziałuX, dokładnośćX);
            liczbaBitówOsobnikaX = ustalLiczbęBitów(długośćPrzedziałuX);
            nudBitKrzyżowaniaX.Maximum = liczbaBitówOsobnikaX;
            nudBitMutowaniaX.Maximum = liczbaBitówOsobnikaX;
        }

        private void nudDokładnośćX_ValueChanged(object sender, EventArgs e)
        {
            dokładnośćX = Convert.ToInt32(nudDokładnośćX.Value);
            długośćPrzedziałuX = ustalDługośćPrzedziału(początekPrzedziałuX, koniecPrzedziałuX, dokładnośćX);
            liczbaBitówOsobnikaX = ustalLiczbęBitów(długośćPrzedziałuX);
            nudBitKrzyżowaniaX.Maximum = liczbaBitówOsobnikaX;
            nudBitMutowaniaX.Maximum = liczbaBitówOsobnikaX;
        }

        private void nudDokładnośćY_Leave(object sender, EventArgs e)
        {
            dokładnośćY = Convert.ToInt32(nudDokładnośćY.Value);
            długośćPrzedziałuY = ustalDługośćPrzedziału(początekPrzedziałuY, koniecPrzedziałuY, dokładnośćY);
            liczbaBitówOsobnikaY = ustalLiczbęBitów(długośćPrzedziałuY);
            nudBitKrzyżowaniaY.Maximum = liczbaBitówOsobnikaY;
            nudBitMutowaniaY.Maximum = liczbaBitówOsobnikaY;
        }

        private void nudDokładnośćY_ValueChanged(object sender, EventArgs e)
        {
            dokładnośćY = Convert.ToInt32(nudDokładnośćY.Value);
            długośćPrzedziałuY = ustalDługośćPrzedziału(początekPrzedziałuY, koniecPrzedziałuY, dokładnośćY);
            liczbaBitówOsobnikaY = ustalLiczbęBitów(długośćPrzedziałuY);
            nudBitKrzyżowaniaY.Maximum = liczbaBitówOsobnikaY;
            nudBitMutowaniaY.Maximum = liczbaBitówOsobnikaY;
        }



        private void nudBitKrzyżowaniaX_Leave(object sender, EventArgs e)
        {
            bitKrzyżowaniaX = Convert.ToInt32(nudBitKrzyżowaniaX.Value);
        }

        private void nudBitKrzyżowaniaY_Leave(object sender, EventArgs e)
        {
            bitKrzyżowaniaY = Convert.ToInt32(nudBitKrzyżowaniaY.Value);
        }


        private void nudBitMutowaniaX_Leave(object sender, EventArgs e)
        {
            bitMutowaniaX = Convert.ToInt32(nudBitMutowaniaX.Value);
        }

        private void nudBitMutowaniaY_Leave(object sender, EventArgs e)
        {
            bitMutowaniaY = Convert.ToInt32(nudBitMutowaniaY.Value);
        }


        private void nudBitKrzyżowaniaX_ValueChanged(object sender, EventArgs e)
        {
            bitKrzyżowaniaX = Convert.ToInt32(nudBitKrzyżowaniaX.Value);
        }

        private void nudBitKrzyżowaniaY_ValueChanged(object sender, EventArgs e)
        {
            bitKrzyżowaniaY = Convert.ToInt32(nudBitKrzyżowaniaY.Value);
        }


        private void nudBitMutowaniaX_ValueChanged(object sender, EventArgs e)
        {
            bitMutowaniaX = Convert.ToInt32(nudBitMutowaniaX.Value);
        }

        private void nudBitMutowaniaY_ValueChanged(object sender, EventArgs e)
        {
            bitMutowaniaY = Convert.ToInt32(nudBitMutowaniaY.Value);
        }


        private void nudPrawdopodobienstwoKrzyzowaniaX_Leave(object sender, EventArgs e)
        {
            prawdopodobieństwoKrzyżowaniaX = Convert.ToInt32(nudPrawdopodobienstwoKrzyzowaniaX.Value);
        }

        private void nudPrawdopodobienstwoKrzyzowaniaX_ValueChanged(object sender, EventArgs e)
        {
            prawdopodobieństwoKrzyżowaniaX = Convert.ToInt32(nudPrawdopodobienstwoKrzyzowaniaX.Value);
        }

        private void nudPrawdopodobienstwoKrzyzowaniaY_Leave(object sender, EventArgs e)
        {
            prawdopodobieństwoKrzyżowaniaY = Convert.ToInt32(nudPrawdopodobienstwoKrzyzowaniaY.Value);
        }

        private void nudPrawdopodobienstwoKrzyzowaniaY_ValueChanged(object sender, EventArgs e)
        {
            prawdopodobieństwoKrzyżowaniaY = Convert.ToInt32(nudPrawdopodobienstwoKrzyzowaniaY.Value);
        }


        private void nudPrawdopodobienstwoMutowaniaX_Leave(object sender, EventArgs e)
        {
            prawdopodobieństwoMutowaniaX = Convert.ToInt32(nudPrawdopodobienstwoMutowaniaX.Value);
        }

        private void nudPrawdopodobienstwoMutowaniaX_ValueChanged(object sender, EventArgs e)
        {
            prawdopodobieństwoMutowaniaX = Convert.ToInt32(nudPrawdopodobienstwoMutowaniaX.Value);
        }

        private void nudPrawdopodobienstwoMutowaniaY_Leave(object sender, EventArgs e)
        {
            prawdopodobieństwoMutowaniaY = Convert.ToInt32(nudPrawdopodobienstwoMutowaniaY.Value);
        }

        private void nudPrawdopodobienstwoMutowaniaY_ValueChanged(object sender, EventArgs e)
        {
            prawdopodobieństwoMutowaniaY = Convert.ToInt32(nudPrawdopodobienstwoMutowaniaY.Value);
        }
        #endregion


        private void chbKontynuuj_CheckedChanged(object sender, EventArgs e)
        {
            if (chbKontynuuj.Checked)
            {
                tbxFunkcjaA.Enabled = false;
                tbxFunkcjaPotęgaX.Enabled = false;
                tbxFunkcjaPotęgaY.Enabled = false;
                btnFunkcjaDodaj.Enabled = false;
                btnFunkcjaCofnij.Enabled = false;

                nudLiczbaOsobnikow.Enabled = false;
                nudDokładnośćX.Enabled = false;
                nudDokładnośćY.Enabled = false;

                tbxPoczątekPrzedziałuX.Enabled = false;
                tbxKoniecPrzedziałuX.Enabled = false;
                tbxPoczątekPrzedziałuY.Enabled = false;
                tbxKoniecPrzedziałuY.Enabled = false;
            }
            else
            {
                chbKontynuuj.Enabled = false;
                chbKontynuuj.Visible = false;

                tbxFunkcjaA.Enabled = true;
                tbxFunkcjaPotęgaX.Enabled = true;
                tbxFunkcjaPotęgaY.Enabled = true;
                btnFunkcjaDodaj.Enabled = true;
                btnFunkcjaCofnij.Enabled = true;

                nudLiczbaOsobnikow.Enabled = true;
                nudDokładnośćX.Enabled = true;
                nudDokładnośćY.Enabled = true;

                tbxPoczątekPrzedziałuX.Enabled = true;
                tbxKoniecPrzedziałuX.Enabled = true;
                tbxPoczątekPrzedziałuY.Enabled = true;
                tbxKoniecPrzedziałuY.Enabled = true;
            }
        }

        private void Menu_Główne_SizeChanged(object sender, EventArgs e)
        {
            rtbWyniki.Height = this.Height - rtbWyniki.Top - 50;
            rtbWyniki.Width = this.Width - rtbWyniki.Left - 20;
        }

        private void btnZapisz_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdZapisz = new SaveFileDialog();
            sfdZapisz.Filter = "Pliki .txt|*.txt";
            sfdZapisz.ShowDialog();
            if (sfdZapisz.FileName != "") rtbWyniki.SaveFile(sfdZapisz.FileName, RichTextBoxStreamType.PlainText);
        }


        #endregion



        #region Metody Genetyczne

        private void generujPopulację(out List<Osobnik> populacja)
        {
            populacja = new List<Osobnik>();

            for (int i = 1; i <= liczbaOsobniów; i++)
            {
                //Osobnik nowyOsobnik = new Osobnik(i, (długośćPrzedziałuX / (liczbaOsobniów + 1)) * i, (długośćPrzedziałuX/(liczbaOsobniów+1))*i);
                
                double noweX, noweY;
                int minLosowania = Convert.ToInt32(początekPrzedziałuX*Math.Pow(10, dokładnośćX));
                int maxLosowania = Convert.ToInt32(koniecPrzedziałuX * Math.Pow(10, dokładnośćX));
                    
                noweX = losuj.Next(minLosowania, maxLosowania);
                noweX /= Math.Pow(10, dokładnośćX);

                minLosowania = Convert.ToInt32(początekPrzedziałuY*Math.Pow(10, dokładnośćY));
                maxLosowania = Convert.ToInt32(koniecPrzedziałuY*Math.Pow(10, dokładnośćY));
                    
                noweY = losuj.Next(minLosowania, maxLosowania);
                noweY /= Math.Pow(10, dokładnośćY);

                Osobnik nowyOsobnik = new Osobnik(i, noweX, noweY);

                populacja.Add(nowyOsobnik);

                if (i == 1)
                {
                    najlepszyOsobnik = new Osobnik(nowyOsobnik);
                    najlepszyOsobnik.obliczPrzystosowanie();
                }
            }
        }

        private void generujPopulację(List<Osobnik> populacja, List<Klucz_Wartość> listaWybrańców)
        {
            populacja.Sort( (x, y) => x.Id.CompareTo(y.Id) );
            listaWybrańców.Sort( (x, y) => x.Wartość.CompareTo(y.Wartość) );

            Klucz_Wartość[] tablicaWybrańców = listaWybrańców.ToArray();
            int i = 0;

            foreach(Osobnik osobnik in populacja)
            {
                if(osobnik.Id == tablicaWybrańców[i].Wartość) //osobnik zostaje
                {
                    if(i+1 < tablicaWybrańców.Length) //zabezpieczenie przed wyjściem poza tablice
                    {
                        i++;
                    }
                }
                else //osobnik jest zastępowany nowym
                {
                    double noweX, noweY;
                    int minLosowania = Convert.ToInt32(początekPrzedziałuX*Math.Pow(10, dokładnośćX));
                    int maxLosowania = Convert.ToInt32(koniecPrzedziałuX * Math.Pow(10, dokładnośćX));
                    
                    noweX = losuj.Next(minLosowania, maxLosowania);
                    noweX /= Math.Pow(10, dokładnośćX);

                    minLosowania = Convert.ToInt32(początekPrzedziałuY*Math.Pow(10, dokładnośćY));
                    maxLosowania = Convert.ToInt32(koniecPrzedziałuY*Math.Pow(10, dokładnośćY));
                    
                    noweY = losuj.Next(minLosowania, maxLosowania);
                    noweY /= Math.Pow(10, dokładnośćY);

                    osobnik.X_WartośćRzeczywista = noweX;
                    osobnik.Y_WartośćRzeczywista = noweY;
                }
            }
        }


        private void selekcja(out List<Klucz_Wartość> idWybranychOsobników, List<Osobnik> populacja)
        {
            idWybranychOsobników = new List<Klucz_Wartość>();

            podliczPrzystosowanieWpopulacji(populacja);

            Klucz_Wartość pozycja;
            int klucz = 1;
            int startoweId = 1;

            if (liczbaOsobniów % 2 == 1)
            {
                Osobnik kandydat1 = populacja.Find(osobnik => osobnik.Id == 1);
                Osobnik kandydat2 = populacja.Find(osobnik => osobnik.Id == 2);
                Osobnik kandydat3 = populacja.Find(osobnik => osobnik.Id == 3);

                if (kandydat1.Przystosowanie > kandydat2.Przystosowanie)
                {
                    if (kandydat1.Przystosowanie > kandydat3.Przystosowanie)
                    {
                        pozycja = new Klucz_Wartość(klucz, kandydat1.Id);
                    }
                    else
                    {
                        pozycja = new Klucz_Wartość(klucz, kandydat3.Id);
                    }
                }
                else
                {
                    if (kandydat2.Przystosowanie > kandydat3.Przystosowanie)
                    {
                        pozycja = new Klucz_Wartość(klucz, kandydat2.Id);
                    }
                    else
                    {
                        pozycja = new Klucz_Wartość(klucz, kandydat3.Id);
                    }
                }

                idWybranychOsobników.Add(pozycja);
                klucz++;
                startoweId = 4;
            }
            
            for (int i = startoweId; i < liczbaOsobniów; i += 2, klucz++)
            {
                Osobnik kandydat1 = populacja.Find(osobnik => osobnik.Id == i);
                Osobnik kandydat2 = populacja.Find(osobnik => osobnik.Id == i+1);
                if (kandydat1.Przystosowanie > kandydat2.Przystosowanie)
                {
                    pozycja = new Klucz_Wartość(klucz, kandydat1.Id);
                }
                else
                {
                    pozycja = new Klucz_Wartość(klucz, kandydat2.Id);
                }

                idWybranychOsobników.Add(pozycja);
            }
        }


        private void krzyżowanie(List<Klucz_Wartość> listaIdWybranychOsobników, List<Osobnik> populacja)
        {
            int idRozpatrzanegoOsobnika = 1;
            if (listaIdWybranychOsobników.Count % 2 == 1)
            {//bierzemy pierwszych 3
                Osobnik osobnik1 = populacja.Find( osobnik => osobnik.Id == listaIdWybranychOsobników.Find(element => element.Klucz == 1).Wartość );
                Osobnik osobnik2 = populacja.Find( osobnik => osobnik.Id == listaIdWybranychOsobników.Find(element => element.Klucz == 2).Wartość );
                Osobnik osobnik3 = populacja.Find( osobnik => osobnik.Id == listaIdWybranychOsobników.Find(element => element.Klucz == 3).Wartość );

                Osobnik tmp = new Osobnik(osobnik1);

                if(czyDokonaćOperacjiGenetycznej(prawdopodobieństwoKrzyżowaniaX))
                {
                    krzyżujXjednego(osobnik1, osobnik2);
                    krzyżujXjednego(osobnik2, osobnik3);
                    krzyżujXjednego(osobnik3, tmp);
                }

                if(czyDokonaćOperacjiGenetycznej(prawdopodobieństwoKrzyżowaniaY))
                {
                    krzyżujYjednego(osobnik1, osobnik2);
                    krzyżujYjednego(osobnik2, osobnik3);
                    krzyżujYjednego(osobnik3, tmp);
                }

                idRozpatrzanegoOsobnika = 4;
            }

            for(int i = idRozpatrzanegoOsobnika; i<listaIdWybranychOsobników.Count; i+=2)
            {
                Osobnik osobnik1 = populacja.Find( osobnik => osobnik.Id == listaIdWybranychOsobników.Find(element => element.Klucz == i).Wartość );
                Osobnik osobnik2 = populacja.Find( osobnik => osobnik.Id == listaIdWybranychOsobników.Find(element => element.Klucz == i+1).Wartość );

                if (czyDokonaćOperacjiGenetycznej(prawdopodobieństwoKrzyżowaniaX))
                {
                    krzyżujX(osobnik1, osobnik2);
                }
                if (czyDokonaćOperacjiGenetycznej(prawdopodobieństwoKrzyżowaniaY))
                {
                    krzyżujY(osobnik1, osobnik2);
                }
            }
        }


        private void mutacja(List<Klucz_Wartość> listaIdWybranychOsobników, List<Osobnik> populacja)
        {
            foreach(Klucz_Wartość element in listaIdWybranychOsobników)
            {
                
                Osobnik osobnik = populacja.Find(o => o.Id == element.Wartość);

                if (czyDokonaćOperacjiGenetycznej(prawdopodobieństwoMutowaniaX))
                {
                    string wynikMutowania = "";

                    //przepisz początek
                    for (int i = 0; i < liczbaBitówOsobnikaX - bitMutowaniaX; i++)
                    {
                        wynikMutowania += osobnik.X_WartośćZakodowanaBinarna[i];
                    }

                    //mutuj
                    if (osobnik.X_WartośćZakodowanaBinarna[liczbaBitówOsobnikaX - bitMutowaniaX] == '0')
                    {
                        wynikMutowania += '1';
                    }
                    else
                    {
                        wynikMutowania += '0';
                    }

                    //przepisz koniec
                    for (int i = liczbaBitówOsobnikaX - bitMutowaniaX + 1; i < liczbaBitówOsobnikaX; i++)
                    {
                        wynikMutowania += osobnik.X_WartośćZakodowanaBinarna[i];
                    }

                    osobnik.X_WartośćZakodowanaBinarna = wynikMutowania;
                }

                if (czyDokonaćOperacjiGenetycznej(prawdopodobieństwoMutowaniaY))
                {
                    string wynikMutowania = "";

                    //przepisz początek
                    for (int i = 0; i < liczbaBitówOsobnikaY - bitMutowaniaY; i++)
                    {
                        wynikMutowania += osobnik.Y_WartośćZakodowanaBinarna[i];
                    }

                    //mutuj
                    if (osobnik.Y_WartośćZakodowanaBinarna[liczbaBitówOsobnikaY - bitMutowaniaY] == '0')
                    {
                        wynikMutowania += '1';
                    }
                    else
                    {
                        wynikMutowania += '0';
                    }

                    //przepisz koniec
                    for (int i = liczbaBitówOsobnikaY - bitMutowaniaY + 1; i < liczbaBitówOsobnikaY; i++)
                    {
                        wynikMutowania += osobnik.Y_WartośćZakodowanaBinarna[i];
                    }

                    osobnik.Y_WartośćZakodowanaBinarna = wynikMutowania;
                }
            }
        }


        private void krzyżujXjednego(Osobnik osobnikDocelowy, Osobnik osobnikKońcówka)
        {
            string wynikKrzyżowania = "";
            for(int i = 0; i<liczbaBitówOsobnikaX - bitKrzyżowaniaX; i++)
            {
                wynikKrzyżowania += osobnikDocelowy.X_WartośćZakodowanaBinarna[i];
            }
            for(int i = liczbaBitówOsobnikaX - bitKrzyżowaniaX; i<liczbaBitówOsobnikaX; i++)
            {
                wynikKrzyżowania += osobnikKońcówka.X_WartośćZakodowanaBinarna[i];
            }

            osobnikDocelowy.X_WartośćZakodowanaBinarna=wynikKrzyżowania;
        }

        private void krzyżujYjednego(Osobnik osobnikDocelowy, Osobnik osobnikKońcówka)
        {
            string wynikKrzyżowania = "";
            for (int i = 0; i < liczbaBitówOsobnikaY - bitKrzyżowaniaY; i++)
            {
                wynikKrzyżowania += osobnikDocelowy.Y_WartośćZakodowanaBinarna[i];
            }
            for (int i = liczbaBitówOsobnikaY - bitKrzyżowaniaY; i < liczbaBitówOsobnikaY; i++)
            {
                wynikKrzyżowania += osobnikKońcówka.Y_WartośćZakodowanaBinarna[i];
            }

            osobnikDocelowy.Y_WartośćZakodowanaBinarna = wynikKrzyżowania;
        }

        private void krzyżujX(Osobnik osobnik1, Osobnik osobnik2)
        {
            string wynikKrzyżowaniaDla1 = "", wynikKrzyżowaniaDla2 = "";

            for(int i = 0; i<liczbaBitówOsobnikaX - bitKrzyżowaniaX; i++)
            {
                wynikKrzyżowaniaDla1 += osobnik1.X_WartośćZakodowanaBinarna[i];
                wynikKrzyżowaniaDla2 += osobnik2.X_WartośćZakodowanaBinarna[i];
            }
            for (int i = liczbaBitówOsobnikaX - bitKrzyżowaniaX; i < liczbaBitówOsobnikaX; i++)
            {
                wynikKrzyżowaniaDla1 += osobnik2.X_WartośćZakodowanaBinarna[i];
                wynikKrzyżowaniaDla2 += osobnik1.X_WartośćZakodowanaBinarna[i];
            }

            osobnik1.X_WartośćZakodowanaBinarna = wynikKrzyżowaniaDla1;
            osobnik2.X_WartośćZakodowanaBinarna = wynikKrzyżowaniaDla2;
        }

        private void krzyżujY(Osobnik osobnik1, Osobnik osobnik2)
        {
            string wynikKrzyżowaniaDla1 = "", wynikKrzyżowaniaDla2 = "";

            for (int i = 0; i < liczbaBitówOsobnikaY - bitKrzyżowaniaY; i++)
            {
                wynikKrzyżowaniaDla1 += osobnik1.Y_WartośćZakodowanaBinarna[i];
                wynikKrzyżowaniaDla2 += osobnik2.Y_WartośćZakodowanaBinarna[i];
            }
            for (int i = liczbaBitówOsobnikaY - bitKrzyżowaniaY; i < liczbaBitówOsobnikaY; i++)
            {
                wynikKrzyżowaniaDla1 += osobnik2.Y_WartośćZakodowanaBinarna[i];
                wynikKrzyżowaniaDla2 += osobnik1.Y_WartośćZakodowanaBinarna[i];
            }

            osobnik1.Y_WartośćZakodowanaBinarna = wynikKrzyżowaniaDla1;
            osobnik2.Y_WartośćZakodowanaBinarna = wynikKrzyżowaniaDla2;
        }


        public static bool czyDokonaćOperacjiGenetycznej(int zdefiniowanePrawdopodobieństwo)
        {
            //Random losuj = new Random();
            if (losuj.Next(1, 100) <= zdefiniowanePrawdopodobieństwo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void podliczPrzystosowanieWpopulacji(List<Osobnik> populacja)
        {
            foreach (Osobnik osobnik in populacja)
            {
                osobnik.obliczPrzystosowanie();
                if (osobnik.Przystosowanie > najlepszyOsobnik.Przystosowanie)
                {
                    najlepszyOsobnik = new Osobnik(osobnik);
                }
            }
        } 

        #endregion



        #region Metody Pomocnicze

        private void inicjalizujZmienne()
        {
            populacja = new List<Osobnik>();
            początekPrzedziałuX = Convert.ToDouble(tbxPoczątekPrzedziałuX.Text);
            koniecPrzedziałuX = Convert.ToDouble(tbxKoniecPrzedziałuX.Text);
            początekPrzedziałuY = Convert.ToDouble(tbxPoczątekPrzedziałuY.Text);
            koniecPrzedziałuY = Convert.ToDouble(tbxKoniecPrzedziałuY.Text);
            czyPoprawnyPoczątekPrzedziałuX = true;
            czyPoprawnyKoniecPrzedziałuX = true;
            czyPoprawnyPoczątekPrzedziałuY = true;
            czyPoprawnyKoniecPrzedziałuY = true;
            liczbaOsobniów = Convert.ToInt32(nudLiczbaOsobnikow.Value);
            liczbaIteracji = Convert.ToInt32(nudLiczbaIteracji.Value);
            dokładnośćX = Convert.ToInt32(nudDokładnośćX.Value);
            dokładnośćY = Convert.ToInt32(nudDokładnośćY.Value);
            bitKrzyżowaniaX = Convert.ToInt32(nudBitKrzyżowaniaX.Value);
            bitKrzyżowaniaY = Convert.ToInt32(nudBitKrzyżowaniaY.Value);
            bitMutowaniaX = Convert.ToInt32(nudBitMutowaniaX.Value);
            bitMutowaniaY = Convert.ToInt32(nudBitMutowaniaY.Value);
            prawdopodobieństwoKrzyżowaniaX = 50;
            prawdopodobieństwoKrzyżowaniaY = 50;
            prawdopodobieństwoMutowaniaX = 50;
            prawdopodobieństwoMutowaniaY = 50;

            lblFunkcja.Text = "???";
            któryToSkładnikFunkcji = 0;

            długośćPrzedziałuX = ustalDługośćPrzedziału(początekPrzedziałuX, koniecPrzedziałuX, dokładnośćX);
            długośćPrzedziałuY = ustalDługośćPrzedziału(początekPrzedziałuY, koniecPrzedziałuY, dokładnośćY);

            liczbaBitówOsobnikaX = ustalLiczbęBitów(długośćPrzedziałuX);
            liczbaBitówOsobnikaY = ustalLiczbęBitów(długośćPrzedziałuY);

            liczbaWykonanychIteracji = 0;
        }

        private int ustalDługośćPrzedziału(double początekPrzedziału, double koniecPrzedziału, int dokładność)
        {
            int długośćPrzedziału;
            długośćPrzedziału = Convert.ToInt32(( Math.Abs(koniecPrzedziału - początekPrzedziału) ) * Math.Pow(10, dokładność));
            długośćPrzedziału++; //żeby koniec przedziału też należał

            return długośćPrzedziału;
        }

        private int ustalLiczbęBitów(int długośćPrzedziału)
        {
            if (długośćPrzedziału == 1) //zabezpieczenie przed błędem w liczeniu logarytmu
            {
                return 1;
            }

            int liczbaBitówOsobnika;
            double testowanaWartość = Math.Log(długośćPrzedziału, 2);
            liczbaBitówOsobnika = Convert.ToInt32(Math.Ceiling(testowanaWartość));

            /*
            if (testowanaWartość - liczbaBitówOsobnika == 0) //zabezpieczenie przed sytuacją gdy zabraknie kodów na 1 liczbę
            {
                liczbaBitówOsobnika++;
            }
             */
            return liczbaBitówOsobnika;
        }

        private bool testujPoczątekPrzedziału(TextBox testowanyTextbox, double koniecPrzedziału)
        {
            bool czyPoprawnyPoczątek;
            double testowanaWartość;
            if (double.TryParse(testowanyTextbox.Text, out testowanaWartość))
            {
                if (testowanaWartość > koniecPrzedziału)
                {
                    czyPoprawnyPoczątek = false;
                }
                else
                {
                    czyPoprawnyPoczątek = true;
                }
            }
            else
            {
                czyPoprawnyPoczątek = false;
            }

            if (!czyPoprawnyPoczątek)
            {
                MessageBox.Show("Wartość nieprawidłowa");
                testowanyTextbox.Focus();
                testowanyTextbox.SelectAll();
            }
            return czyPoprawnyPoczątek;
        }

        private bool testujKoniecPrzedziału(TextBox testowanyTextbox, double początekPrzedziału)
        {
            bool czyKoniecPoprawny;
            double testowanaWartość;
            if (double.TryParse(testowanyTextbox.Text, out testowanaWartość))
            {
                if (testowanaWartość < początekPrzedziału)
                {
                    czyKoniecPoprawny = false;
                }
                else
                {
                    czyKoniecPoprawny = true;
                }
            }
            else
            {
                
                czyKoniecPoprawny = false;
            }

            if (!czyKoniecPoprawny)
            {
                MessageBox.Show("Wartość nieprawidłowa");
                testowanyTextbox.Focus();
                testowanyTextbox.SelectAll();
            }
            return czyKoniecPoprawny;
        }

        private bool czyWszystkieDanePoprawne()
        {
            //dopisywać
            if (czyPoprawnyPoczątekPrzedziałuX && czyPoprawnyKoniecPrzedziałuX && czyPoprawnyPoczątekPrzedziałuY && czyPoprawnyKoniecPrzedziałuY)
                return true;
            else
                return false;
        }

        private void dopiszDoRichTexta(List<Osobnik> populacja, int numerIteracji)
        {
            double maxX=0, maxY=0, maxF=0;
            bool pierwszy = true;
            rtbWyniki.Text += "---------------------------------------------------------------------------------\n";
            rtbWyniki.Text += "|   Pokolenie " + numerIteracji.ToString() + ":\t\t\t\t|\n";
            rtbWyniki.Text += "---------------------------------------------------------------------------------\n";
            foreach(Osobnik osobnik in populacja)
            {
                osobnik.obliczPrzystosowanie();
                if (pierwszy)
                {
                    pierwszy = false;
                    maxX = osobnik.X_WartośćRzeczywista;
                    maxY = osobnik.Y_WartośćRzeczywista;
                    maxF = osobnik.Przystosowanie;
                }
                else
                {
                    if (osobnik.Przystosowanie > maxF)
                    {
                        maxX = osobnik.X_WartośćRzeczywista;
                        maxY = osobnik.Y_WartośćRzeczywista;
                        maxF = osobnik.Przystosowanie;
                    }
                }

                rtbWyniki.Text += "Osobnik " + osobnik.Id + ":\nx = " + /*Math.Round(*/osobnik.X_WartośćRzeczywista/*, dokładnośćX)*/.ToString() + "\t\t" + osobnik.X_WartośćZakodowanaBinarna;
                
                rtbWyniki.Text += "\ny = " + /*Math.Round(*/osobnik.Y_WartośćRzeczywista/*, dokładnośćY)*/.ToString() + "\t\t" + osobnik.Y_WartośćZakodowanaBinarna + "\nf(x,y)=" + osobnik.Przystosowanie.ToString() + "\n\n";

                
            }
            rtbWyniki.Text += "______________________________\n";
            rtbWyniki.Text += "Najlepsze wartości w tym pokoleniu:\n\tx=" + maxX.ToString() + "\n\ty=" + maxY.ToString() + "\n\tf(x,y)=" + maxF.ToString() + "\n\n\n";
        }

        private bool czyJestLiczbąRzeczywistą(string testowanaWartość)
        {
            double x;
            if (Double.TryParse(testowanaWartość, out x))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string kopiujStringa(string tekst, int start, int koniec)
        {
            string nowyTekst = "";
            for (int i = start; i <= koniec; i++)
            {
                nowyTekst += tekst[i];
            }
            return nowyTekst;
        }

        public static double obliczWartośćFunkcji(double x, double y, List<Składnik_Funkcji> funkcja)
        {
            double wartośćFunkcji = 0;
            foreach (Składnik_Funkcji składnik in funkcja)
            {
                wartośćFunkcji += składnik.obliczWartośćSkładnika(x, y);
                //wartośćFunkcji += składnik.a * Math.Pow(x, składnik.potęgaX) * Math.Pow(y, składnik.potęgaY);
            }
            return wartośćFunkcji;
        }

        private bool czyNapewnoZmienić()
        {
            if (chbKontynuuj.Checked)
            {//to nie działa
                DialogResult czyNapewnoZmienić = MessageBox.Show("Po wprowadzeniu tej zmiany stracisz możliwość kontynuowania działania algorytmu dla obecnej populacji. Czy chcesz zatwierdzić tą zmianę?", "Ostrzeżenie", MessageBoxButtons.YesNo);
                if (czyNapewnoZmienić == DialogResult.Yes)
                {
                    chbKontynuuj.Checked = false;
                    chbKontynuuj.Enabled = false;
                    chbKontynuuj.Visible = false;
                    return true;
                }
                else if (czyNapewnoZmienić == DialogResult.No)
                {
                    return false;
                }
                else return true; //nigdy się nie wykona
            }
            else
            {
                return true;
            }
        }

        #endregion

       

        

        

        

        
        
        
    }
}
