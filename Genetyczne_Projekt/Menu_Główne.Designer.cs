﻿namespace Genetyczne_Projekt
{
    partial class Menu_Główne
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFunkcja = new System.Windows.Forms.Label();
            this.nudLiczbaOsobnikow = new System.Windows.Forms.NumericUpDown();
            this.lblLiczbaOsobników = new System.Windows.Forms.Label();
            this.lblLiczbaIteracji = new System.Windows.Forms.Label();
            this.nudLiczbaIteracji = new System.Windows.Forms.NumericUpDown();
            this.tbxPoczątekPrzedziałuX = new System.Windows.Forms.TextBox();
            this.lblPoczątekPrzedziałuX = new System.Windows.Forms.Label();
            this.lblKoniecPrzedziałuX = new System.Windows.Forms.Label();
            this.tbxKoniecPrzedziałuX = new System.Windows.Forms.TextBox();
            this.lblDokładnośćX = new System.Windows.Forms.Label();
            this.nudDokładnośćX = new System.Windows.Forms.NumericUpDown();
            this.lblBitKrzyżowaniaX = new System.Windows.Forms.Label();
            this.nudBitKrzyżowaniaX = new System.Windows.Forms.NumericUpDown();
            this.lblBitMutowaniaX = new System.Windows.Forms.Label();
            this.nudBitMutowaniaX = new System.Windows.Forms.NumericUpDown();
            this.btnStart = new System.Windows.Forms.Button();
            this.rtbWyniki = new System.Windows.Forms.RichTextBox();
            this.tbxFunkcjaA = new System.Windows.Forms.TextBox();
            this.tbxFunkcjaPotęgaX = new System.Windows.Forms.TextBox();
            this.tbxFunkcjaPotęgaY = new System.Windows.Forms.TextBox();
            this.lblX = new System.Windows.Forms.Label();
            this.lblY = new System.Windows.Forms.Label();
            this.btnFunkcjaDodaj = new System.Windows.Forms.Button();
            this.btnFunkcjaCofnij = new System.Windows.Forms.Button();
            this.lblFodXY = new System.Windows.Forms.Label();
            this.lblKoniecPrzedziałuY = new System.Windows.Forms.Label();
            this.tbxKoniecPrzedziałuY = new System.Windows.Forms.TextBox();
            this.lblPoczątekPrzedziałuY = new System.Windows.Forms.Label();
            this.tbxPoczątekPrzedziałuY = new System.Windows.Forms.TextBox();
            this.lblBitMutowaniaY = new System.Windows.Forms.Label();
            this.nudBitMutowaniaY = new System.Windows.Forms.NumericUpDown();
            this.lblBitKrzyżowaniaY = new System.Windows.Forms.Label();
            this.nudBitKrzyżowaniaY = new System.Windows.Forms.NumericUpDown();
            this.lblDokładnośćY = new System.Windows.Forms.Label();
            this.nudDokładnośćY = new System.Windows.Forms.NumericUpDown();
            this.lblPrawdopodobienstwoMutowaniaY = new System.Windows.Forms.Label();
            this.nudPrawdopodobienstwoMutowaniaY = new System.Windows.Forms.NumericUpDown();
            this.lblPrawdopodobienstwoKrzyzowaniaY = new System.Windows.Forms.Label();
            this.nudPrawdopodobienstwoKrzyzowaniaY = new System.Windows.Forms.NumericUpDown();
            this.lblPrawdopodobienstwoMutowaniaX = new System.Windows.Forms.Label();
            this.nudPrawdopodobienstwoMutowaniaX = new System.Windows.Forms.NumericUpDown();
            this.lblPrawdopodobienstwoKrzyzowaniaX = new System.Windows.Forms.Label();
            this.nudPrawdopodobienstwoKrzyzowaniaX = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.chbKontynuuj = new System.Windows.Forms.CheckBox();
            this.btnZapisz = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.nudLiczbaOsobnikow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLiczbaIteracji)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDokładnośćX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitKrzyżowaniaX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitMutowaniaX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitMutowaniaY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitKrzyżowaniaY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDokładnośćY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoMutowaniaY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoKrzyzowaniaY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoMutowaniaX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoKrzyzowaniaX)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFunkcja
            // 
            this.lblFunkcja.AutoSize = true;
            this.lblFunkcja.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFunkcja.Location = new System.Drawing.Point(73, 9);
            this.lblFunkcja.Name = "lblFunkcja";
            this.lblFunkcja.Size = new System.Drawing.Size(70, 17);
            this.lblFunkcja.TabIndex = 0;
            this.lblFunkcja.Text = "-x^2-y+4";
            // 
            // nudLiczbaOsobnikow
            // 
            this.nudLiczbaOsobnikow.Location = new System.Drawing.Point(281, 19);
            this.nudLiczbaOsobnikow.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.nudLiczbaOsobnikow.Name = "nudLiczbaOsobnikow";
            this.nudLiczbaOsobnikow.Size = new System.Drawing.Size(39, 20);
            this.nudLiczbaOsobnikow.TabIndex = 1;
            this.nudLiczbaOsobnikow.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudLiczbaOsobnikow.ValueChanged += new System.EventHandler(this.nudLiczbaOsobnikow_ValueChanged);
            this.nudLiczbaOsobnikow.Leave += new System.EventHandler(this.nudLiczbaOsobnikow_Leave);
            // 
            // lblLiczbaOsobników
            // 
            this.lblLiczbaOsobników.AutoSize = true;
            this.lblLiczbaOsobników.Location = new System.Drawing.Point(181, 21);
            this.lblLiczbaOsobników.Name = "lblLiczbaOsobników";
            this.lblLiczbaOsobników.Size = new System.Drawing.Size(94, 13);
            this.lblLiczbaOsobników.TabIndex = 2;
            this.lblLiczbaOsobników.Text = "Liczba Osobników";
            // 
            // lblLiczbaIteracji
            // 
            this.lblLiczbaIteracji.AutoSize = true;
            this.lblLiczbaIteracji.Location = new System.Drawing.Point(10, 21);
            this.lblLiczbaIteracji.Name = "lblLiczbaIteracji";
            this.lblLiczbaIteracji.Size = new System.Drawing.Size(72, 13);
            this.lblLiczbaIteracji.TabIndex = 4;
            this.lblLiczbaIteracji.Text = "Liczba Iteracji";
            // 
            // nudLiczbaIteracji
            // 
            this.nudLiczbaIteracji.Location = new System.Drawing.Point(87, 19);
            this.nudLiczbaIteracji.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nudLiczbaIteracji.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLiczbaIteracji.Name = "nudLiczbaIteracji";
            this.nudLiczbaIteracji.Size = new System.Drawing.Size(39, 20);
            this.nudLiczbaIteracji.TabIndex = 3;
            this.nudLiczbaIteracji.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudLiczbaIteracji.Leave += new System.EventHandler(this.nudLiczbaIteracji_Leave);
            // 
            // tbxPoczątekPrzedziałuX
            // 
            this.tbxPoczątekPrzedziałuX.Location = new System.Drawing.Point(117, 13);
            this.tbxPoczątekPrzedziałuX.Name = "tbxPoczątekPrzedziałuX";
            this.tbxPoczątekPrzedziałuX.Size = new System.Drawing.Size(36, 20);
            this.tbxPoczątekPrzedziałuX.TabIndex = 5;
            this.tbxPoczątekPrzedziałuX.Text = "0";
            this.tbxPoczątekPrzedziałuX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPoczątekPrzedziałuX.Leave += new System.EventHandler(this.tbxPoczątekPrzedziałuX_Leave);
            // 
            // lblPoczątekPrzedziałuX
            // 
            this.lblPoczątekPrzedziałuX.AutoSize = true;
            this.lblPoczątekPrzedziałuX.Location = new System.Drawing.Point(6, 16);
            this.lblPoczątekPrzedziałuX.Name = "lblPoczątekPrzedziałuX";
            this.lblPoczątekPrzedziałuX.Size = new System.Drawing.Size(105, 13);
            this.lblPoczątekPrzedziałuX.TabIndex = 6;
            this.lblPoczątekPrzedziałuX.Text = "Początek Przedziału";
            // 
            // lblKoniecPrzedziałuX
            // 
            this.lblKoniecPrzedziałuX.AutoSize = true;
            this.lblKoniecPrzedziałuX.Location = new System.Drawing.Point(6, 38);
            this.lblKoniecPrzedziałuX.Name = "lblKoniecPrzedziałuX";
            this.lblKoniecPrzedziałuX.Size = new System.Drawing.Size(93, 13);
            this.lblKoniecPrzedziałuX.TabIndex = 8;
            this.lblKoniecPrzedziałuX.Text = "Koniec Przedziału";
            // 
            // tbxKoniecPrzedziałuX
            // 
            this.tbxKoniecPrzedziałuX.Location = new System.Drawing.Point(117, 35);
            this.tbxKoniecPrzedziałuX.Name = "tbxKoniecPrzedziałuX";
            this.tbxKoniecPrzedziałuX.Size = new System.Drawing.Size(36, 20);
            this.tbxKoniecPrzedziałuX.TabIndex = 7;
            this.tbxKoniecPrzedziałuX.Text = "100";
            this.tbxKoniecPrzedziałuX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxKoniecPrzedziałuX.Leave += new System.EventHandler(this.tbxKoniecPrzedziałuX_Leave);
            // 
            // lblDokładnośćX
            // 
            this.lblDokładnośćX.AutoSize = true;
            this.lblDokładnośćX.Location = new System.Drawing.Point(6, 60);
            this.lblDokładnośćX.Name = "lblDokładnośćX";
            this.lblDokładnośćX.Size = new System.Drawing.Size(66, 13);
            this.lblDokładnośćX.TabIndex = 10;
            this.lblDokładnośćX.Text = "Dokładność";
            // 
            // nudDokładnośćX
            // 
            this.nudDokładnośćX.Location = new System.Drawing.Point(117, 58);
            this.nudDokładnośćX.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudDokładnośćX.Name = "nudDokładnośćX";
            this.nudDokładnośćX.Size = new System.Drawing.Size(39, 20);
            this.nudDokładnośćX.TabIndex = 9;
            this.nudDokładnośćX.ValueChanged += new System.EventHandler(this.nudDokładnośćX_ValueChanged);
            this.nudDokładnośćX.Leave += new System.EventHandler(this.nudDokładnośćX_Leave);
            // 
            // lblBitKrzyżowaniaX
            // 
            this.lblBitKrzyżowaniaX.AutoSize = true;
            this.lblBitKrzyżowaniaX.Location = new System.Drawing.Point(6, 21);
            this.lblBitKrzyżowaniaX.Name = "lblBitKrzyżowaniaX";
            this.lblBitKrzyżowaniaX.Size = new System.Drawing.Size(81, 13);
            this.lblBitKrzyżowaniaX.TabIndex = 12;
            this.lblBitKrzyżowaniaX.Text = "Bit Krzyżowania";
            // 
            // nudBitKrzyżowaniaX
            // 
            this.nudBitKrzyżowaniaX.Location = new System.Drawing.Point(99, 19);
            this.nudBitKrzyżowaniaX.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudBitKrzyżowaniaX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBitKrzyżowaniaX.Name = "nudBitKrzyżowaniaX";
            this.nudBitKrzyżowaniaX.Size = new System.Drawing.Size(39, 20);
            this.nudBitKrzyżowaniaX.TabIndex = 11;
            this.nudBitKrzyżowaniaX.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudBitKrzyżowaniaX.ValueChanged += new System.EventHandler(this.nudBitKrzyżowaniaX_ValueChanged);
            this.nudBitKrzyżowaniaX.Leave += new System.EventHandler(this.nudBitKrzyżowaniaX_Leave);
            // 
            // lblBitMutowaniaX
            // 
            this.lblBitMutowaniaX.AutoSize = true;
            this.lblBitMutowaniaX.Location = new System.Drawing.Point(6, 45);
            this.lblBitMutowaniaX.Name = "lblBitMutowaniaX";
            this.lblBitMutowaniaX.Size = new System.Drawing.Size(74, 13);
            this.lblBitMutowaniaX.TabIndex = 14;
            this.lblBitMutowaniaX.Text = "Bit Mutowania";
            // 
            // nudBitMutowaniaX
            // 
            this.nudBitMutowaniaX.Location = new System.Drawing.Point(99, 43);
            this.nudBitMutowaniaX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBitMutowaniaX.Name = "nudBitMutowaniaX";
            this.nudBitMutowaniaX.Size = new System.Drawing.Size(39, 20);
            this.nudBitMutowaniaX.TabIndex = 13;
            this.nudBitMutowaniaX.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudBitMutowaniaX.ValueChanged += new System.EventHandler(this.nudBitMutowaniaX_ValueChanged);
            this.nudBitMutowaniaX.Leave += new System.EventHandler(this.nudBitMutowaniaX_Leave);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(221, 54);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 15;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // rtbWyniki
            // 
            this.rtbWyniki.Location = new System.Drawing.Point(373, 41);
            this.rtbWyniki.Name = "rtbWyniki";
            this.rtbWyniki.Size = new System.Drawing.Size(320, 546);
            this.rtbWyniki.TabIndex = 16;
            this.rtbWyniki.Text = "";
            // 
            // tbxFunkcjaA
            // 
            this.tbxFunkcjaA.Location = new System.Drawing.Point(25, 39);
            this.tbxFunkcjaA.Name = "tbxFunkcjaA";
            this.tbxFunkcjaA.Size = new System.Drawing.Size(20, 20);
            this.tbxFunkcjaA.TabIndex = 17;
            // 
            // tbxFunkcjaPotęgaX
            // 
            this.tbxFunkcjaPotęgaX.Location = new System.Drawing.Point(59, 20);
            this.tbxFunkcjaPotęgaX.Name = "tbxFunkcjaPotęgaX";
            this.tbxFunkcjaPotęgaX.Size = new System.Drawing.Size(20, 20);
            this.tbxFunkcjaPotęgaX.TabIndex = 18;
            // 
            // tbxFunkcjaPotęgaY
            // 
            this.tbxFunkcjaPotęgaY.Location = new System.Drawing.Point(90, 20);
            this.tbxFunkcjaPotęgaY.Name = "tbxFunkcjaPotęgaY";
            this.tbxFunkcjaPotęgaY.Size = new System.Drawing.Size(20, 20);
            this.tbxFunkcjaPotęgaY.TabIndex = 19;
            // 
            // lblX
            // 
            this.lblX.AutoSize = true;
            this.lblX.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblX.Location = new System.Drawing.Point(47, 40);
            this.lblX.Name = "lblX";
            this.lblX.Size = new System.Drawing.Size(14, 17);
            this.lblX.TabIndex = 20;
            this.lblX.Text = "x";
            // 
            // lblY
            // 
            this.lblY.AutoSize = true;
            this.lblY.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblY.Location = new System.Drawing.Point(79, 40);
            this.lblY.Name = "lblY";
            this.lblY.Size = new System.Drawing.Size(15, 17);
            this.lblY.TabIndex = 21;
            this.lblY.Text = "y";
            // 
            // btnFunkcjaDodaj
            // 
            this.btnFunkcjaDodaj.Location = new System.Drawing.Point(9, 65);
            this.btnFunkcjaDodaj.Name = "btnFunkcjaDodaj";
            this.btnFunkcjaDodaj.Size = new System.Drawing.Size(52, 23);
            this.btnFunkcjaDodaj.TabIndex = 22;
            this.btnFunkcjaDodaj.Text = "Dodaj";
            this.btnFunkcjaDodaj.UseVisualStyleBackColor = true;
            this.btnFunkcjaDodaj.Click += new System.EventHandler(this.btnFunkcjaDodaj_Click);
            // 
            // btnFunkcjaCofnij
            // 
            this.btnFunkcjaCofnij.Location = new System.Drawing.Point(67, 65);
            this.btnFunkcjaCofnij.Name = "btnFunkcjaCofnij";
            this.btnFunkcjaCofnij.Size = new System.Drawing.Size(52, 23);
            this.btnFunkcjaCofnij.TabIndex = 23;
            this.btnFunkcjaCofnij.Text = "Cofnij";
            this.btnFunkcjaCofnij.UseVisualStyleBackColor = true;
            this.btnFunkcjaCofnij.Click += new System.EventHandler(this.btnFunkcjaCofnij_Click);
            // 
            // lblFodXY
            // 
            this.lblFodXY.AutoSize = true;
            this.lblFodXY.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblFodXY.Location = new System.Drawing.Point(12, 9);
            this.lblFodXY.Name = "lblFodXY";
            this.lblFodXY.Size = new System.Drawing.Size(64, 17);
            this.lblFodXY.TabIndex = 24;
            this.lblFodXY.Text = "f(x,y) = ";
            // 
            // lblKoniecPrzedziałuY
            // 
            this.lblKoniecPrzedziałuY.AutoSize = true;
            this.lblKoniecPrzedziałuY.Location = new System.Drawing.Point(6, 39);
            this.lblKoniecPrzedziałuY.Name = "lblKoniecPrzedziałuY";
            this.lblKoniecPrzedziałuY.Size = new System.Drawing.Size(93, 13);
            this.lblKoniecPrzedziałuY.TabIndex = 28;
            this.lblKoniecPrzedziałuY.Text = "Koniec Przedziału";
            // 
            // tbxKoniecPrzedziałuY
            // 
            this.tbxKoniecPrzedziałuY.Location = new System.Drawing.Point(117, 36);
            this.tbxKoniecPrzedziałuY.Name = "tbxKoniecPrzedziałuY";
            this.tbxKoniecPrzedziałuY.Size = new System.Drawing.Size(36, 20);
            this.tbxKoniecPrzedziałuY.TabIndex = 27;
            this.tbxKoniecPrzedziałuY.Text = "100";
            this.tbxKoniecPrzedziałuY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxKoniecPrzedziałuY.Leave += new System.EventHandler(this.tbxKoniecPrzedziałuY_Leave);
            // 
            // lblPoczątekPrzedziałuY
            // 
            this.lblPoczątekPrzedziałuY.AutoSize = true;
            this.lblPoczątekPrzedziałuY.Location = new System.Drawing.Point(6, 17);
            this.lblPoczątekPrzedziałuY.Name = "lblPoczątekPrzedziałuY";
            this.lblPoczątekPrzedziałuY.Size = new System.Drawing.Size(105, 13);
            this.lblPoczątekPrzedziałuY.TabIndex = 26;
            this.lblPoczątekPrzedziałuY.Text = "Początek Przedziału";
            // 
            // tbxPoczątekPrzedziałuY
            // 
            this.tbxPoczątekPrzedziałuY.Location = new System.Drawing.Point(117, 14);
            this.tbxPoczątekPrzedziałuY.Name = "tbxPoczątekPrzedziałuY";
            this.tbxPoczątekPrzedziałuY.Size = new System.Drawing.Size(36, 20);
            this.tbxPoczątekPrzedziałuY.TabIndex = 25;
            this.tbxPoczątekPrzedziałuY.Text = "0";
            this.tbxPoczątekPrzedziałuY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbxPoczątekPrzedziałuY.Leave += new System.EventHandler(this.tbxPoczątekPrzedziałuY_Leave);
            // 
            // lblBitMutowaniaY
            // 
            this.lblBitMutowaniaY.AutoSize = true;
            this.lblBitMutowaniaY.Location = new System.Drawing.Point(6, 46);
            this.lblBitMutowaniaY.Name = "lblBitMutowaniaY";
            this.lblBitMutowaniaY.Size = new System.Drawing.Size(74, 13);
            this.lblBitMutowaniaY.TabIndex = 36;
            this.lblBitMutowaniaY.Text = "Bit Mutowania";
            // 
            // nudBitMutowaniaY
            // 
            this.nudBitMutowaniaY.Location = new System.Drawing.Point(99, 44);
            this.nudBitMutowaniaY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBitMutowaniaY.Name = "nudBitMutowaniaY";
            this.nudBitMutowaniaY.Size = new System.Drawing.Size(39, 20);
            this.nudBitMutowaniaY.TabIndex = 35;
            this.nudBitMutowaniaY.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nudBitMutowaniaY.ValueChanged += new System.EventHandler(this.nudBitMutowaniaY_ValueChanged);
            this.nudBitMutowaniaY.Leave += new System.EventHandler(this.nudBitMutowaniaY_Leave);
            // 
            // lblBitKrzyżowaniaY
            // 
            this.lblBitKrzyżowaniaY.AutoSize = true;
            this.lblBitKrzyżowaniaY.Location = new System.Drawing.Point(6, 21);
            this.lblBitKrzyżowaniaY.Name = "lblBitKrzyżowaniaY";
            this.lblBitKrzyżowaniaY.Size = new System.Drawing.Size(81, 13);
            this.lblBitKrzyżowaniaY.TabIndex = 34;
            this.lblBitKrzyżowaniaY.Text = "Bit Krzyżowania";
            // 
            // nudBitKrzyżowaniaY
            // 
            this.nudBitKrzyżowaniaY.Location = new System.Drawing.Point(99, 19);
            this.nudBitKrzyżowaniaY.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudBitKrzyżowaniaY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudBitKrzyżowaniaY.Name = "nudBitKrzyżowaniaY";
            this.nudBitKrzyżowaniaY.Size = new System.Drawing.Size(39, 20);
            this.nudBitKrzyżowaniaY.TabIndex = 33;
            this.nudBitKrzyżowaniaY.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.nudBitKrzyżowaniaY.ValueChanged += new System.EventHandler(this.nudBitKrzyżowaniaY_ValueChanged);
            this.nudBitKrzyżowaniaY.Leave += new System.EventHandler(this.nudBitKrzyżowaniaY_Leave);
            // 
            // lblDokładnośćY
            // 
            this.lblDokładnośćY.AutoSize = true;
            this.lblDokładnośćY.Location = new System.Drawing.Point(6, 61);
            this.lblDokładnośćY.Name = "lblDokładnośćY";
            this.lblDokładnośćY.Size = new System.Drawing.Size(66, 13);
            this.lblDokładnośćY.TabIndex = 32;
            this.lblDokładnośćY.Text = "Dokładność";
            // 
            // nudDokładnośćY
            // 
            this.nudDokładnośćY.Location = new System.Drawing.Point(117, 59);
            this.nudDokładnośćY.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudDokładnośćY.Name = "nudDokładnośćY";
            this.nudDokładnośćY.Size = new System.Drawing.Size(39, 20);
            this.nudDokładnośćY.TabIndex = 31;
            this.nudDokładnośćY.ValueChanged += new System.EventHandler(this.nudDokładnośćY_ValueChanged);
            this.nudDokładnośćY.Leave += new System.EventHandler(this.nudDokładnośćY_Leave);
            // 
            // lblPrawdopodobienstwoMutowaniaY
            // 
            this.lblPrawdopodobienstwoMutowaniaY.AutoSize = true;
            this.lblPrawdopodobienstwoMutowaniaY.Location = new System.Drawing.Point(6, 94);
            this.lblPrawdopodobienstwoMutowaniaY.Name = "lblPrawdopodobienstwoMutowaniaY";
            this.lblPrawdopodobienstwoMutowaniaY.Size = new System.Drawing.Size(164, 13);
            this.lblPrawdopodobienstwoMutowaniaY.TabIndex = 44;
            this.lblPrawdopodobienstwoMutowaniaY.Text = "Prawdopodobieństwo Mutowania";
            // 
            // nudPrawdopodobienstwoMutowaniaY
            // 
            this.nudPrawdopodobienstwoMutowaniaY.Location = new System.Drawing.Point(184, 92);
            this.nudPrawdopodobienstwoMutowaniaY.Name = "nudPrawdopodobienstwoMutowaniaY";
            this.nudPrawdopodobienstwoMutowaniaY.Size = new System.Drawing.Size(39, 20);
            this.nudPrawdopodobienstwoMutowaniaY.TabIndex = 43;
            this.nudPrawdopodobienstwoMutowaniaY.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudPrawdopodobienstwoMutowaniaY.ValueChanged += new System.EventHandler(this.nudPrawdopodobienstwoMutowaniaY_ValueChanged);
            this.nudPrawdopodobienstwoMutowaniaY.Leave += new System.EventHandler(this.nudPrawdopodobienstwoMutowaniaY_Leave);
            // 
            // lblPrawdopodobienstwoKrzyzowaniaY
            // 
            this.lblPrawdopodobienstwoKrzyzowaniaY.AutoSize = true;
            this.lblPrawdopodobienstwoKrzyzowaniaY.Location = new System.Drawing.Point(6, 70);
            this.lblPrawdopodobienstwoKrzyzowaniaY.Name = "lblPrawdopodobienstwoKrzyzowaniaY";
            this.lblPrawdopodobienstwoKrzyzowaniaY.Size = new System.Drawing.Size(171, 13);
            this.lblPrawdopodobienstwoKrzyzowaniaY.TabIndex = 42;
            this.lblPrawdopodobienstwoKrzyzowaniaY.Text = "Prawdopodobieństwo Krzyżowania";
            // 
            // nudPrawdopodobienstwoKrzyzowaniaY
            // 
            this.nudPrawdopodobienstwoKrzyzowaniaY.Location = new System.Drawing.Point(184, 68);
            this.nudPrawdopodobienstwoKrzyzowaniaY.Name = "nudPrawdopodobienstwoKrzyzowaniaY";
            this.nudPrawdopodobienstwoKrzyzowaniaY.Size = new System.Drawing.Size(39, 20);
            this.nudPrawdopodobienstwoKrzyzowaniaY.TabIndex = 41;
            this.nudPrawdopodobienstwoKrzyzowaniaY.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudPrawdopodobienstwoKrzyzowaniaY.ValueChanged += new System.EventHandler(this.nudPrawdopodobienstwoKrzyzowaniaY_ValueChanged);
            this.nudPrawdopodobienstwoKrzyzowaniaY.Leave += new System.EventHandler(this.nudPrawdopodobienstwoKrzyzowaniaY_Leave);
            // 
            // lblPrawdopodobienstwoMutowaniaX
            // 
            this.lblPrawdopodobienstwoMutowaniaX.AutoSize = true;
            this.lblPrawdopodobienstwoMutowaniaX.Location = new System.Drawing.Point(6, 93);
            this.lblPrawdopodobienstwoMutowaniaX.Name = "lblPrawdopodobienstwoMutowaniaX";
            this.lblPrawdopodobienstwoMutowaniaX.Size = new System.Drawing.Size(164, 13);
            this.lblPrawdopodobienstwoMutowaniaX.TabIndex = 40;
            this.lblPrawdopodobienstwoMutowaniaX.Text = "Prawdopodobieństwo Mutowania";
            // 
            // nudPrawdopodobienstwoMutowaniaX
            // 
            this.nudPrawdopodobienstwoMutowaniaX.Location = new System.Drawing.Point(184, 91);
            this.nudPrawdopodobienstwoMutowaniaX.Name = "nudPrawdopodobienstwoMutowaniaX";
            this.nudPrawdopodobienstwoMutowaniaX.Size = new System.Drawing.Size(39, 20);
            this.nudPrawdopodobienstwoMutowaniaX.TabIndex = 39;
            this.nudPrawdopodobienstwoMutowaniaX.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudPrawdopodobienstwoMutowaniaX.ValueChanged += new System.EventHandler(this.nudPrawdopodobienstwoMutowaniaX_ValueChanged);
            this.nudPrawdopodobienstwoMutowaniaX.Leave += new System.EventHandler(this.nudPrawdopodobienstwoMutowaniaX_Leave);
            // 
            // lblPrawdopodobienstwoKrzyzowaniaX
            // 
            this.lblPrawdopodobienstwoKrzyzowaniaX.AutoSize = true;
            this.lblPrawdopodobienstwoKrzyzowaniaX.Location = new System.Drawing.Point(6, 70);
            this.lblPrawdopodobienstwoKrzyzowaniaX.Name = "lblPrawdopodobienstwoKrzyzowaniaX";
            this.lblPrawdopodobienstwoKrzyzowaniaX.Size = new System.Drawing.Size(171, 13);
            this.lblPrawdopodobienstwoKrzyzowaniaX.TabIndex = 38;
            this.lblPrawdopodobienstwoKrzyzowaniaX.Text = "Prawdopodobieństwo Krzyżowania";
            // 
            // nudPrawdopodobienstwoKrzyzowaniaX
            // 
            this.nudPrawdopodobienstwoKrzyzowaniaX.Location = new System.Drawing.Point(184, 67);
            this.nudPrawdopodobienstwoKrzyzowaniaX.Name = "nudPrawdopodobienstwoKrzyzowaniaX";
            this.nudPrawdopodobienstwoKrzyzowaniaX.Size = new System.Drawing.Size(39, 20);
            this.nudPrawdopodobienstwoKrzyzowaniaX.TabIndex = 37;
            this.nudPrawdopodobienstwoKrzyzowaniaX.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.nudPrawdopodobienstwoKrzyzowaniaX.ValueChanged += new System.EventHandler(this.nudPrawdopodobienstwoKrzyzowaniaX_ValueChanged);
            this.nudPrawdopodobienstwoKrzyzowaniaX.Leave += new System.EventHandler(this.nudPrawdopodobienstwoKrzyzowaniaX_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblPoczątekPrzedziałuX);
            this.groupBox1.Controls.Add(this.lblKoniecPrzedziałuX);
            this.groupBox1.Controls.Add(this.tbxPoczątekPrzedziałuX);
            this.groupBox1.Controls.Add(this.tbxKoniecPrzedziałuX);
            this.groupBox1.Controls.Add(this.lblDokładnośćX);
            this.groupBox1.Controls.Add(this.nudDokładnośćX);
            this.groupBox1.Location = new System.Drawing.Point(9, 43);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(163, 87);
            this.groupBox1.TabIndex = 46;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Przedziały dla X";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblPoczątekPrzedziałuY);
            this.groupBox2.Controls.Add(this.lblKoniecPrzedziałuY);
            this.groupBox2.Controls.Add(this.lblDokładnośćY);
            this.groupBox2.Controls.Add(this.tbxPoczątekPrzedziałuY);
            this.groupBox2.Controls.Add(this.tbxKoniecPrzedziałuY);
            this.groupBox2.Controls.Add(this.nudDokładnośćY);
            this.groupBox2.Location = new System.Drawing.Point(181, 43);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(163, 87);
            this.groupBox2.TabIndex = 47;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Przedziały dla Y";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Location = new System.Drawing.Point(16, 320);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(256, 278);
            this.groupBox3.TabIndex = 48;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ustawienia Genetyczne";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lblBitKrzyżowaniaY);
            this.groupBox5.Controls.Add(this.nudBitKrzyżowaniaY);
            this.groupBox5.Controls.Add(this.nudBitMutowaniaY);
            this.groupBox5.Controls.Add(this.lblBitMutowaniaY);
            this.groupBox5.Controls.Add(this.lblPrawdopodobienstwoKrzyzowaniaY);
            this.groupBox5.Controls.Add(this.lblPrawdopodobienstwoMutowaniaY);
            this.groupBox5.Controls.Add(this.nudPrawdopodobienstwoKrzyzowaniaY);
            this.groupBox5.Controls.Add(this.nudPrawdopodobienstwoMutowaniaY);
            this.groupBox5.Location = new System.Drawing.Point(9, 148);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(234, 119);
            this.groupBox5.TabIndex = 50;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Dla Y";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lblBitKrzyżowaniaX);
            this.groupBox4.Controls.Add(this.nudBitKrzyżowaniaX);
            this.groupBox4.Controls.Add(this.nudBitMutowaniaX);
            this.groupBox4.Controls.Add(this.lblBitMutowaniaX);
            this.groupBox4.Controls.Add(this.nudPrawdopodobienstwoKrzyzowaniaX);
            this.groupBox4.Controls.Add(this.lblPrawdopodobienstwoKrzyzowaniaX);
            this.groupBox4.Controls.Add(this.nudPrawdopodobienstwoMutowaniaX);
            this.groupBox4.Controls.Add(this.lblPrawdopodobienstwoMutowaniaX);
            this.groupBox4.Location = new System.Drawing.Point(9, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(232, 119);
            this.groupBox4.TabIndex = 49;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dla X";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tbxFunkcjaPotęgaX);
            this.groupBox6.Controls.Add(this.tbxFunkcjaA);
            this.groupBox6.Controls.Add(this.tbxFunkcjaPotęgaY);
            this.groupBox6.Controls.Add(this.lblX);
            this.groupBox6.Controls.Add(this.lblY);
            this.groupBox6.Controls.Add(this.btnFunkcjaDodaj);
            this.groupBox6.Controls.Add(this.btnFunkcjaCofnij);
            this.groupBox6.Location = new System.Drawing.Point(16, 41);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(127, 100);
            this.groupBox6.TabIndex = 49;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Edycja Funkcji";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.groupBox2);
            this.groupBox7.Controls.Add(this.groupBox1);
            this.groupBox7.Controls.Add(this.lblLiczbaIteracji);
            this.groupBox7.Controls.Add(this.nudLiczbaIteracji);
            this.groupBox7.Controls.Add(this.nudLiczbaOsobnikow);
            this.groupBox7.Controls.Add(this.lblLiczbaOsobników);
            this.groupBox7.Location = new System.Drawing.Point(16, 156);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(351, 141);
            this.groupBox7.TabIndex = 50;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Ustawienia Główne";
            // 
            // chbKontynuuj
            // 
            this.chbKontynuuj.AutoSize = true;
            this.chbKontynuuj.Location = new System.Drawing.Point(177, 85);
            this.chbKontynuuj.Name = "chbKontynuuj";
            this.chbKontynuuj.Size = new System.Drawing.Size(176, 17);
            this.chbKontynuuj.TabIndex = 51;
            this.chbKontynuuj.Text = "Kontynuuj dla obecnej populacji";
            this.chbKontynuuj.UseVisualStyleBackColor = true;
            this.chbKontynuuj.CheckedChanged += new System.EventHandler(this.chbKontynuuj_CheckedChanged);
            // 
            // btnZapisz
            // 
            this.btnZapisz.Location = new System.Drawing.Point(209, 111);
            this.btnZapisz.Name = "btnZapisz";
            this.btnZapisz.Size = new System.Drawing.Size(95, 23);
            this.btnZapisz.TabIndex = 52;
            this.btnZapisz.Text = "Zapisz Wyniki";
            this.btnZapisz.UseVisualStyleBackColor = true;
            this.btnZapisz.Click += new System.EventHandler(this.btnZapisz_Click);
            // 
            // Menu_Główne
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 599);
            this.Controls.Add(this.btnZapisz);
            this.Controls.Add(this.chbKontynuuj);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lblFodXY);
            this.Controls.Add(this.rtbWyniki);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.lblFunkcja);
            this.Name = "Menu_Główne";
            this.Text = "Poszukiwacz Maksimum Funkcji";
            this.SizeChanged += new System.EventHandler(this.Menu_Główne_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.nudLiczbaOsobnikow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLiczbaIteracji)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDokładnośćX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitKrzyżowaniaX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitMutowaniaX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitMutowaniaY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBitKrzyżowaniaY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDokładnośćY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoMutowaniaY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoKrzyzowaniaY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoMutowaniaX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPrawdopodobienstwoKrzyzowaniaX)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFunkcja;
        private System.Windows.Forms.NumericUpDown nudLiczbaOsobnikow;
        private System.Windows.Forms.Label lblLiczbaOsobników;
        private System.Windows.Forms.Label lblLiczbaIteracji;
        private System.Windows.Forms.NumericUpDown nudLiczbaIteracji;
        private System.Windows.Forms.TextBox tbxPoczątekPrzedziałuX;
        private System.Windows.Forms.Label lblPoczątekPrzedziałuX;
        private System.Windows.Forms.Label lblKoniecPrzedziałuX;
        private System.Windows.Forms.TextBox tbxKoniecPrzedziałuX;
        private System.Windows.Forms.Label lblDokładnośćX;
        private System.Windows.Forms.NumericUpDown nudDokładnośćX;
        private System.Windows.Forms.Label lblBitKrzyżowaniaX;
        private System.Windows.Forms.NumericUpDown nudBitKrzyżowaniaX;
        private System.Windows.Forms.Label lblBitMutowaniaX;
        private System.Windows.Forms.NumericUpDown nudBitMutowaniaX;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.RichTextBox rtbWyniki;
        private System.Windows.Forms.TextBox tbxFunkcjaA;
        private System.Windows.Forms.TextBox tbxFunkcjaPotęgaX;
        private System.Windows.Forms.TextBox tbxFunkcjaPotęgaY;
        private System.Windows.Forms.Label lblX;
        private System.Windows.Forms.Label lblY;
        private System.Windows.Forms.Button btnFunkcjaDodaj;
        private System.Windows.Forms.Button btnFunkcjaCofnij;
        private System.Windows.Forms.Label lblFodXY;
        private System.Windows.Forms.Label lblKoniecPrzedziałuY;
        private System.Windows.Forms.TextBox tbxKoniecPrzedziałuY;
        private System.Windows.Forms.Label lblPoczątekPrzedziałuY;
        private System.Windows.Forms.TextBox tbxPoczątekPrzedziałuY;
        private System.Windows.Forms.Label lblBitMutowaniaY;
        private System.Windows.Forms.NumericUpDown nudBitMutowaniaY;
        private System.Windows.Forms.Label lblBitKrzyżowaniaY;
        private System.Windows.Forms.NumericUpDown nudBitKrzyżowaniaY;
        private System.Windows.Forms.Label lblDokładnośćY;
        private System.Windows.Forms.NumericUpDown nudDokładnośćY;
        private System.Windows.Forms.Label lblPrawdopodobienstwoMutowaniaY;
        private System.Windows.Forms.NumericUpDown nudPrawdopodobienstwoMutowaniaY;
        private System.Windows.Forms.Label lblPrawdopodobienstwoKrzyzowaniaY;
        private System.Windows.Forms.NumericUpDown nudPrawdopodobienstwoKrzyzowaniaY;
        private System.Windows.Forms.Label lblPrawdopodobienstwoMutowaniaX;
        private System.Windows.Forms.NumericUpDown nudPrawdopodobienstwoMutowaniaX;
        private System.Windows.Forms.Label lblPrawdopodobienstwoKrzyzowaniaX;
        private System.Windows.Forms.NumericUpDown nudPrawdopodobienstwoKrzyzowaniaX;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox chbKontynuuj;
        private System.Windows.Forms.Button btnZapisz;
    }
}

